﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using UnityEngine;

public class CalcDistFromCursor : MonoBehaviour
{
    public CanvasGroup uiElement;   

    public int distThreshold = 100;

    public int alphaGainMult = 5;

    void Start()
    {
   
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePos = Input.mousePosition;
        Vector2 objPos = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);
        var distance = Vector2.Distance(mousePos, objPos);
       
        if (distance <= distThreshold)
        {
            uiElement.alpha = alphaGainMult/distance;
        }
        else 
        {
            uiElement.alpha = 0.0f;
        }
    }
}
