﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;


public class WebpageOpener : MonoBehaviour
{
    public string url;

    public void OpenWebpage()
    {
    #if !UNITY_EDITOR
        openWindow(url);
        return;
    #endif
    
    Application.OpenURL(url);
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
