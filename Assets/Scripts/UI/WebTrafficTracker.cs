﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WebTrafficTracker : MonoBehaviour
{
    int linkCounter = 0;

    public TextMeshProUGUI counterText;

    public void IncrementCounter()
    {
        linkCounter += 1;
        counterText.SetText(linkCounter + " Hits");
    }
}
