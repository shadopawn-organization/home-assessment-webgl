﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteScript : MonoBehaviour
{
    public Component script;

    public void Destroy()
    {
        Component.Destroy(script);
    }
}
