﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WorldTimer : MonoBehaviour
{
    private float totalTime = 0.0f;

    public TextMeshProUGUI timerText;

    void Start()
    {
        
    }

    
    void Update()
    {
        totalTime += Time.deltaTime;
        timerText.SetText(totalTime.ToString());

    }
}
