﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using TMPro;

public class HeartTracker : MonoBehaviour
{
    HeartToggle[] heartToggles;

    float heartPercent = 0.0f;

    public TextMeshProUGUI percentText;

    void Start()
    {
        heartToggles = Resources.FindObjectsOfTypeAll<HeartToggle>();
    }

    public void UpdateHeartPercent()
    {
        int toggleTally = 0;

        foreach(HeartToggle toggle in heartToggles)
        {
            if (toggle.isToggled)
            {
                toggleTally += 1;
            }
        }

        toggleTally *= 100;
        int toggleLength = heartToggles.Length * 100;
        heartPercent = (int)Math.Round((double)(100 * toggleTally) / toggleLength);
        percentText.SetText(heartPercent.ToString() + "%");
    }
}
