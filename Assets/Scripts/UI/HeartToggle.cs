﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class HeartToggle : MonoBehaviour
{
    public Sprite altSprite;

    public Button button;

    public bool isToggled = false;

    private UnityEvent HeartChange = new UnityEvent();

    public void ToggleSprite()
    {
        if (isToggled == false)
        {
            button.image.overrideSprite = altSprite;

            isToggled = true;
        }
        else
        {
            button.image.overrideSprite = null;
            isToggled = false;
        }

        HeartChange.Invoke();
    }
}
